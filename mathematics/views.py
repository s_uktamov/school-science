from django.shortcuts import render, get_list_or_404
from .models import Science

# Create your views here.
def all_lesson(request):
    sciences = Science.objects.all()
    return render(request,'mathematics/lessons.html', {'sciences': sciences})

def lesson(request,lesson_id):
    sciencess = get_list_or_404(Science,pk=lesson_id)
    print(len(sciencess),'bittaaaaa')
    return render(request,'mathematics/lesson.html',{'sciencess': sciencess})
