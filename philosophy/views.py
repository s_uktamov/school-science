from django.shortcuts import render, get_list_or_404
from .models import SciencePhilosophy

def philosophy_all_lesson(request):
    sciences = SciencePhilosophy.objects.all()
    return render(request,'philosophy/lessons.html', {'sciences': sciences})

def philosophy_lesson(request,lesson_id):
    sciencess = get_list_or_404(SciencePhilosophy,pk=lesson_id)
    return render(request,'philosophy/lesson.html',{'sciencess': sciencess})
