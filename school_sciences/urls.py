
from django.contrib import admin
from django.urls import path, include
from home.views import all_science
from philosophy.views import philosophy_all_lesson

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path('mathematics/',include('mathematics.urls')),
    path('philosophy/',include('philosophy.urls'))
]
