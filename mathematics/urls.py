
from django.urls import path
from .views import all_lesson, lesson

app_name = 'mathematics'

urlpatterns = [
    path('', all_lesson, name='all_lesson'),
    path('lesson/<int:lesson_id>', lesson, name='lesson') 
]