from django.urls import path
from .views import all_science

urlpatterns = [
    path('', all_science, name='all_science')
]