from django.db import models

# Create your models here.
class SciencePhilosophy(models.Model):
    lesson = models.CharField(max_length=150)
    teacher = models.CharField(max_length=20)
    date = models.DateField()
    description = models.TextField()
    url = models.URLField(blank=True)