from django.urls import path
from .views import philosophy_all_lesson, philosophy_lesson

app_name = 'philosophy'

urlpatterns = [
    path('', philosophy_all_lesson, name='philosophy_all_lesson'),
    path('philosophy_lesson/<int:lesson_id>', philosophy_lesson, name='philosophy_lesson') 
] 